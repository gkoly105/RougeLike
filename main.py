
import random
import time
import tkinter
from tkinter import *

npsive = []
thingsive = []
plasive = []
npcnum = 0
attacks = []
attacks2 = []
crafts = ["Камень", "Уголь", "Железо", "Алмаз", "Золото", "Ткань", "Камыш", "Глина", "Кирпич", "Адамант",
          "Брусок дерева", "Обсидиан", "Камень душ"]
foods = ["Яблоко", "Сырое мясо животных", "Ягоды", "Орехи", "Хлеб", "Пирог", "Суп", "Рыба", "Суши", "Водоросли",
         "Жареное мясо животных", "Пшеница", "Сырой пирог"]
foods2 = [["Яблоко", 15], ["Сырое мясо животных", 20], ["Ягоды", 10], ["Орехи", 10], ["Хлеб", 35], ["Пирог", 45],
          ["Суп", 85], ["Рыба", 25], ["Суши", 50], ["Водоросли", 10], ["Жареное мясо животных", 45], ["Пшеница", 10],
          ["Сырой пирог", 20]]
shelds = [["Броня воина",2],["Броня паладиина",4],["Броня рыцаря",3],["Броня адаманта",6],["Одежда",1],["Броня лесника",2],["Броня шахтера",6],["Броня тени",10]]
uses = ["Кирка", "Топор", "Молот", "Удочка"]
weapons = ["Меч", "Алебарда", "Лук", "Кинжал", "Посох", "Дубинка", "Порох", "Мушкет"]
weapons2 = [["Меч", 5], ["Алебарда", 10], ["Лук", 6], ["Кинжал", 3], ["Посох", 4], ["Дубинка", 4], ["Порох", 6],
            ["Мушкет", 8]]
magics = ["Заклинаниие поджога", "Заклинаниие взрыва", "Заклинаниие заморозки", "Заклинаниие вакуума",
          "Заклинаниие смерти", "Заклинаниие яда", "Заклинаниие слепоты", "Заклинаниие аннигиляции"]
magicsx = ["Зелье поджога", "Зелье взрыва", "Зелье заморозки", "Зелье вакуума",
          "Зелье смерти", "Зелье яда", "Зелье слепоты", "Зелье аннигиляции"]

binds = [["Брусок дерева", "Камень", "Кирка"],["Железо", "Кинжал", "Топор"],
         ["Брусок дерева", "Дубинка", "Молот"],
         ["Железо", "Ткань", "Часть брони воина"],
         ["Алмаз", "Ткань", "Часть брони паладиина"],
         ["Золото", "Ткань", "Часть брони рыцаря"],
         ["Адамант", "Ткань", "Часть брони адаманта"],
         ["Адамант", "Алмаз", "Основа зелья"],
         ["Заклинаниие поджога", "Основа зелья", "Зелье поджога"],
         ["Заклинаниие взрыва", "Основа зелья", "Зелье взрыва"], ["Заклинаниие заморозки", "Основа зелья", "Зелье заморозки"],
         ["Заклинаниие вакуума", "Основа зелья", "Зелье вакуума"], ["Заклинаниие смерти", "Основа зелья", "Зелье смерти"],
         ["Заклинаниие яда", "Основа зелья", "Зелье яда"], ["Заклинаниие слепоты", "Основа зелья", "Зелье слепоты"],
         ["Заклинаниие аннигиляции", "Основа зелья", "Зелье аннигиляции"],
         ["Ткань", "Ткань", "Часть одежды"],
         ["Брусок дерева", "Ткань", "Часть брони лесника"],
         ["Обсидиан", "Ткань", "Часть брони шахтера"],
         ["Камень душ", "Камень душ", "Часть брони тени"],
         ["Часть брони воина", "Часть брони воина", "Броня воина"],
         ["Часть брони паладиина", "Часть брони паладиина", "Броня паладиина"],
         ["Часть брони рыцаря", "Часть брони рыцаря", "Броня рыцаря"],
         ["Часть брони адаманта", "Часть брони адаманта", "Брони адаманта"],
         ["Часть одежды", "Часть одежды", "Одежда"],
         ["Часть брони лесника", "Часть брони лесника", "Броня лесника"],
         ["Часть брони шахтера", "Часть брони шахтера", "Броня шахтера"],
         ["Часть брони тени", "Часть брони тени", "Броня тени"],
         ["Камыш", "Камыш", "Ткань"],["Ткань", "Аура угля", "Заклинаниие поджога"],
         ["Ткань", "Аура пороха", "Заклинаниие взрыва"], ["Ткань", "Аура алмаза", "Заклинаниие заморозки"],
         ["Ткань", "Аура глины", "Заклинаниие вакуума"], ["Ткань", "Аура адаманта", "Заклинаниие смерти"],
         ["Ткань", "Аура камыша", "Заклинаниие яда"], ["Ткань", "Аура обсидиана", "Заклинаниие слепоты"],
         ["Ткань", "Аура золота", "Заклинаниие аннигиляции"], ["Брусок дерева", "Кинжал", "Меч"],
         ["Меч", "Железо", "Алебарда"], ["Брусок дерева", "Камыш", "Лук"], ["Брусок дерева", "Железо", "Кинжал"],
         ["Брусок дерева", "Брусок дерева", "Посох"], ["Брусок дерева", "Глина", "Дубинка"],
         ["Ткань", "Уголь", "Порох"], ["Железо", "Порох", "Мушкет"],
         ["Сырое мясо животных", "Уголь", "Жареное мясо животных"],["Сырое мясо животных", "Брусок дерева", "Жареное мясо животных"],
         ["Яблоко", "Пшеница", "Сырой пирог"],
         ["Сырой пирог", "Уголь", "Пирог"], ["Сырое мясо животных", "Рыба", "Суп"], ["Пшеница", "Уголь", "Хлеб"],
         ["Рыба", "Водоросли", "Суши"], ["Адамант", "Обсидиан", "Камень душ"], ["Глина", "Уголь", "Кирпич"],
         ["Уголь", "Камень душ", "Аура угля"], ["Порох", "Камень душ", "Аура пороха"],
         ["Алмаз", "Камень душ", "Аура алмаза"], ["Глина", "Камень душ", "Аура глины"],
         ["Адамант", "Камень душ", "Аура адаманта"], ["Камыш", "Камень душ", "Аура камыша"],
         ["Обсидиан", "Камень душ", "Аура обсидиана"], ["Золото", "Камень душ", "Аура золота"], ["Брусок дерева", "Брусок дерева", "Сундук"], ["Камыш", "Глина", "Замазка"],
         ["Камыш", "Ткань", "Парус"],["Брусок дерева", "Замазка", "Корпус лодки"],["Корпус лодки", "Парус", "Лодка"]]

findable = ["Камень", "Уголь", "Железо", "Алмаз", "Золото", "Ткань", "Камыш", "Глина", "Кирпич", "Адамант",
            "Брусок дерева", "Обсидиан", "Яблоко", "Сырое мясо животных", "Ягоды", "Орехи", "Водоросли", "Пшеница",
            "Кинжал"]
mount = ["Камень", "Уголь", "Железо", "Глина"]
rare = ["Алмаз", "Золото", "Адамант", "Обсидиан"]
forest = ["Камень", "Брусок дерева", "Камыш", "Глина", "Яблоко", "Ягоды", "Орехи"]
lug = ["Ягоды", "Орехи", "Пшеница"]
water = ["Рыба", "Суши", "Камыш"]


class Kingdom(object):
    def __init__(self, type2):
        self.type = type2
        self.relations = 0


class Tree(object):
    def __init__(self):
        self.wrd = "T"


class Animal(object):
    def __init__(self, type2,x,y):
        self.wrd = type2
        self.x = x
        self.y = y
        self.life = 0
        self.mana = 0
        if(self.wrd=="f"):
            self.hp = random.randint(5, 30)
            self.dmg = random.randint(0, 3)
        if (self.wrd == "b"):
            self.hp = random.randint(35, 150)
            self.dmg = random.randint(2, 10)
        if (self.wrd == "r"):
            self.hp = random.randint(10, 30)
            self.dmg = random.randint(0, 1)
        if (self.wrd == "m"):
            self.hp = random.randint(35, 120)
            self.dmg = random.randint(1, 8)
        if (self.wrd == "h"):
            self.hp = random.randint(35, 120)
            self.dmg = random.randint(0, 4)

    def pair(self, x, y):
        ma = 0
        if (isinstance(map[x + 1][y][3], Animal)):
            if (map[x + 1][y][3].wrd == self.wrd):
                ma = ma + 1
        if (isinstance(map[x][y + 1][3], Animal)):
            if (map[x][y + 1][3].wrd == self.wrd):
                ma = ma + 1
        if (isinstance(map[x - 1][y][3], Animal)):
            if (map[x - 1][y][3].wrd == self.wrd):
                ma = ma + 1
        if (isinstance(map[x][y - 1][3], Animal)):
            if (map[x][y - 1][3].wrd == self.wrd):
                ma = ma + 1
        if (ma > 0 and ma < 4):
            if (isinstance(map[x + 1][y][3], Animal)):
                map[x + 1][y][3] = Animal(self.wrd,x + 1,y)
            if (isinstance(map[x][y + 1][3], Animal)):
                map[x][y + 1][3] = Animal(self.wrd,x,y + 1)
            if (isinstance(map[x - 1][y][3], Animal)):
                map[x - 1][y][3] = Animal(self.wrd,x - 1,y)
            if (isinstance(map[x][y - 1][3], Animal)):
                map[x][y - 1][3] = Animal(self.wrd,x,y - 1)
        elif (self.life > 5):
            map[x][y][3] = 0
        self.life = self.life + 1


class Wall(object):
    def __init__(self, hp, typex, cas):
        self.hp = hp
        self.typex = typex
        self.wrd = "#"
        self.cas = cas


class House(object):
    def __init__(self, hp, typex):
        self.hp = hp
        self.typex = typex
        self.wrd = "@"

class Chest(object):
    def __init__(self,inv):
        self.invent = []
        self.inv = inv
        self.wrd = "%"

def findinv2(name):
    for klk in shelds:
        if(klk[0] == name):
            return klk[1]
class Thing(object):
    def __init__(self, name, type, attack):
        '''if type==1:
            self.name = weapons[random.randint(0,len(weapons)-1)]
        if type==2:
            self.name = crafts[random.randint(0,len(crafts)-1)]
        if type==3:
            self.name = magics[random.randint(0,len(magics)-1)]
        if type==4:
            self.name = foods[random.randint(0,len(foods)-1)]
        if type==5:
            self.name = weapons[random.randint(0,len(weapons)-1)]'''
        self.type = type
        self.mana = 0
        self.dmg = 1
        self.name = name
        if (type == 1):
            cec2 = 0
            for cec in weapons:
                if (self.name == cec):
                    self.dmg = random.randint(weapons2[cec2][1] - 2, weapons2[cec2][1] + 2)
                cec2 = cec2 + 1
        if (type == 3):
            if(name[1]=='а'):
                self.dmg = random.randint(15, 40)
                self.mana = random.randint(5, self.dmg * 3)
            else:
                self.dmg = random.randint(25, 50)
                self.mana = random.randint(5, self.dmg * 3)
        if (type == 2 or type == 4 or type == 5):
            self.dmg = 1
            self.mana = 0
        if(type==5):
            self.defend = findinv2(self.name)
        self.ener = random.randint(0, self.dmg * 3)
        thingsive.append(self)
        if(type!=5):
            attacks2.append(self)
            attacks.append(attack)


def randthing():
    thin = findable[random.randint(0, len(findable) - 1)]
    if (thin in weapons):
        th = Thing(thin, 1, "Резкая атака ")
    if (thin in foods):
        th = Thing(thin, 4, "Шлепок ")
    if (thin in crafts):
        th = Thing(thin, 1, "Удар ")
    return th


class NPC(object):
    def __init__(self, name, mindbase, hp, mana, relations, type1, x, y, cas):  # Создание неписей
        self.mindbase = mindbase
        self.hp = hp
        self.mana = mana
        self.maxhp = hp
        self.maxmana = mana
        self.relations = relations
        self.type = type1
        self.har = []
        self.point = ""
        self.name = name
        self.buf = 0
        self.x = x
        self.y = y
        self.dmg = random.randint(0, type1 * random.randint(1, 3))
        self.cas = cas
        npsive.append(self)
        self.wrd = "N"

    def createQuest(self, kill, get, place):
        type = random.randint(1, 1)
        main = 0
        qmain = ""
        quest = ""
        qtypes = ["Убить ", "Добыть ", "Попасть в ", "Принести "]
        thing = ""
        if (type == 1):
            main = random.randint(0, len(kill) - 1)
            qmain = kill[main]
            thing = qmain.name
        if (type == 2 or type == 4):
            main = random.randint(0, len(get) - 1)
            qmain = get[main]
            thing = qmain.name
        if (type == 3):
            main = random.randint(0, len(place) - 1)
            qmain = place[main]
            thing = qmain.name
        quest = qtypes[type - 1] + str(thing)
        answ = [quest, thing, qmain]
        return answ

    def logic(self):
        if (isinstance(map[self.x + 1][self.y][4], Hero)):
            if (self.relations < -99):
                map[self.x + 1][self.y][4].fight(self)
        if (isinstance(map[self.x][self.y + 1][4], Hero)):
            if (self.relations < -99):
                map[self.x][self.y + 1][4].fight(self)
        if (isinstance(map[self.x - 1][self.y][4], Hero)):
            if (self.relations < -99):
                map[self.x - 1][self.y][4].fight(self)
        if (isinstance(map[self.x][self.y - 1][4], Hero)):
            if (self.relations < -99):
                map[self.x][self.y - 1][3].fight(self)
        if (kingdoms[self.cas].relations < -50):
            self.relations = -100
        if (self.type == 1):
            if (self.buf == 0):
                if (not isinstance(map[self.x + 1][self.y][3], Wall)):
                    movemap(self.x, self.y, 4, self.x + 1, self.y, 4)
                self.buf = self.buf + 1
            if (self.buf == 1):
                if (not isinstance(map[self.x][self.y - 1][3], Wall)):
                    movemap(self.x, self.y, 4, self.x, self.y + 1, 4)
                self.buf = self.buf + 1
            if (self.buf == 2):
                if (not isinstance(map[self.x - 1][self.y][3], Wall)):
                    movemap(self.x, self.y, 4, self.x - 1, self.y, 4)
                self.buf = self.buf + 1
            if (self.buf == 3):
                if (not isinstance(map[self.x][self.y - 1][3], Wall)):
                    movemap(self.x, self.y, 4, self.x, self.y - 1, 4)
                self.buf = 0

        """if (self.type == 2):
            if (map[self.x + 1][self.y][3] or map[self.x - 1][self.y][3] or map[self.x][self.y + 1][3] == ''):
                movemap(self.x, self.y, 3, self.x, self.y + 2, 3)
            elif (map[self.x + 1][self.y][3] or map[self.x - 1][self.y][3] or map[self.x][self.y + 1][3] or  map[self.x][self.y + 1][3] == '@'):

                if (map[self.x + 1][self.y][3] != "#"):
                    movemap(self.x, self.y, 3, self.x + 1, self.y, 3)
                if (map[self.x - 1][self.y][3] != "#"):
                    movemap(self.x, self.y, 3, self.x - 1, self.y, 3)
                if (map[self.x][self.y - 1][3] != "#"):
                    movemap(self.x, self.y, 3, self.x, self.y - 1, 3)
                if (map[self.x][self.y + 2][3] != "#"):
                    movemap(self.x, self.y, 3, self.x, self.y + 1, 3)"""


class Hero(object):
    def __init__(self, mindbase, hp, mana, typex, classx, inv, ener,x,y):  # Создание игрока
        self.mindbase = mindbase
        self.hp = hp
        self.mana = mana
        self.maxhp = hp
        self.maxmana = mana
        self.ener = ener
        self.maxener = ener
        self.typex = typex
        self.classx = classx
        self.inv = inv
        self.invent = []
        self.x = x
        self.y = y
        self.wrd = "/"
        self.friend = "0"

    def fight(self, who):
        ehp = who.hp
        yes = False
        if (len(qwst) == 3):
            yes = qwst[2] == who
        who.relations = -999
        eman = who.mana
        edmg = who.dmg
        ehp = int(ehp)
        while (self.hp > 0 and ehp > 0):
            print("HP врага - " + str(ehp))
            print("Мана врага - " + str(eman))
            print("Урон врага - " + str(edmg))
            print("HP - " + str(self.hp))
            print("Мана - " + str(self.mana))
            print("Энергия - " + str(self.ener))
            print("Выберете удар:")
            g = 1
            for f in attacks:
                print(str(g) + ") " + str(f) + str(self.invent[g - 1].name) + " с уроном " + str(
                    attacks2[g - 1].dmg) + ", маной " + str(attacks2[g - 1].mana) + " и энергией " + str(
                    attacks2[g - 1].ener))
                g = g + 1
            hit = int(input())
            if (self.ener > attacks2[hit - 1].ener and self.mana > attacks2[hit - 1].mana):
                ehp = ehp - attacks2[hit - 1].dmg
                self.ener = self.ener - attacks2[hit - 1].ener
                self.mana = self.mana - attacks2[hit - 1].mana
            defen = 0
            for dig in self.invent:
                if(dig.type==5):
                    defen = defen + dig.defend
            if(edmg - defen>-1):
                self.hp = self.hp - (edmg - defen)
        if (self.hp > 0):
            print("Вы победиили и нашли трофей!")
            if (isinstance(who, NPC)):
                map[who.x][who.y][4] = 0
                if (len(self.invent) < self.inv):
                    self.invent.append(randthing())
                if (yes):
                    print("Вы выполнили квест!")
                    if (len(self.invent) < self.inv):
                        self.invent.append(randthing())
            if (isinstance(who, Animal)):
                map[who.x][who.y][3] = 0
                th = Thing("Сырое мясо животных", 4, 1)
                if (len(self.invent) < self.inv):
                    self.invent.append(th)
        else:
            print("Вы умерли")
            exit(0)

    def talk(self, whom):
        if (self.mindbase == whom.mindbase):
            whom.relations = whom.relations + 25
        if (abs(self.mindbase - whom.mindbase) > 2):
            whom.relations = whom.relations - 25
        you = ""
        while (whom.relations > -80 and you != "пока"):
            you = input()
            you = you.lower()
            if (you == "дай квест"):
                if (whom.relations > 20):
                    qwst = whom.createQuest(npsive, thingsive, plasive)
                    print(qwst[0])
                else:
                    print("Неа")
            if (you == 'поторгуемся?'or you == 'торговец'):
                frst = randthing()
                scnd = randthing()
                print('Я продаю '+frst.name+' за '+scnd.name)
                if(self.findinv(scnd.name)):
                    popx = str(input("Купить "+frst.name+"?"))
                    if(popx.lower()=="Да"):
                        del self.invent[self.findinv3(scnd.name)]
                        self.invent.append(frst)
            if (you == 'как дела?'):
                if (whom.relations > 0):
                    print('Все хорошо')
                    whom.relations = whom.relations + 5
                else:
                    print('Убирайся!')
                    whom.relations = whom.relations - 5

            if (you == 'что делаешь?'):
                if (whom.relations > 0):
                    print('Ничего')
                    whom.relations = whom.relations + 5
                else:
                    print('Какое тебе дело!')
                    whom.relations = whom.relations - 5
            if (you == 'кто ты?'):
                if (whom.relations > 0):
                    print('Я ' + whom.name)
                    whom.relations = whom.relations + 5
                else:
                    print('Какое тебе дело!')
                    whom.relations = whom.relations - 5
            if (you == 'тебе помочь?'):
                if (whom.relations > 0):
                    print('Спасибо, но я справлюсь сам')
                    whom.relations = whom.relations + 5
                else:
                    print('Нет')
                    whom.relations = whom.relations + 10
            if (you == 'куда идешь?'):
                if (whom.relations > 20):
                    print('Я иду в ...')
                    whom.relations = whom.relations + 8
                else:
                    print('...')
                    whom.relations = whom.relations + 5

    def findinv(self,name):
        for klk in self.invent:
            if(klk.name == name):
                return True
        return False

    def findinv3(self,name):
        bitter = -1
        for klk in self.invent:
            bitter = bitter + 1
            if(klk.name == name):
                return bitter
        return bitter

    def logic(self, com):
        p = []
        q = ""
        com = com.lower()
        for eq in range(0, len(com) - 1):
            if (com[eq] != " "):
                q = q + com[eq]
            else:
                p.append(q)
                q = ""
        q = q + com[len(com) - 1]
        p.append(q)
        if (p[0] == "положить"):
            for f in self.invent:
                print(str(f.name) + " с уроном " + str(f.dmg))
            hjh = int(input())
            opo = self.invent[hjh]
            if (isinstance(map[self.x + 1][self.y][3], Chest)):
                if(len(map[self.x + 1][self.y][3].invent)>map[self.x + 1][self.y][3].inv):
                    map[self.x + 1][self.y][3].invent.append(opo)
                    del self.invent[hjh]
            elif (isinstance(map[self.x - 1][self.y][3], Chest)):
                if (len(map[self.x - 1][self.y][3].invent) > map[self.x - 1][self.y][3].inv):
                    map[self.x - 1][self.y][3].invent.append(opo)
                    del self.invent[hjh]
            elif (isinstance(map[self.x][self.y + 1][3], Chest)):
                if (len(map[self.x][self.y + 1][3].invent) > map[self.x][self.y + 1][3].inv):
                    map[self.x][self.y + 1][3].invent.append(opo)
                    del self.invent[hjh]
            elif (isinstance(map[self.x][self.y - 1][3], Chest)):
                if (len(map[self.x][self.y - 1][3].invent) > map[self.x][self.y - 1][3].inv):
                    map[self.x][self.y - 1][3].invent.append(opo)
                    del self.invent[hjh]
        if (p[0] == "взять"):
            hjh = int(input())
            opo = self.invent[hjh]
            if (isinstance(map[self.x + 1][self.y][3], Chest)):
                for f in map[self.x + 1][self.y][3].invent:
                    print(str(f.name) + " с уроном " + str(f.dmg))
                hjh = int(input())
                opo = map[self.x + 1][self.y][3].invent[hjh]
                if(len(self.invent)<self.inv):
                    self.invent.append(opo)
                    del map[self.x + 1][self.y][3].invent[hjh]
            elif (isinstance(map[self.x - 1][self.y][3], Chest)):
                for f in map[self.x - 1][self.y][3].invent:
                    print(str(f.name) + " с уроном " + str(f.dmg))
                hjh = int(input())
                opo = map[self.x - 1][self.y][3].invent[hjh]
                if (len(self.invent) < self.inv):
                    self.invent.append(opo)
                    del map[self.x - 1][self.y][3].invent[hjh]
            elif (isinstance(map[self.x][self.y + 1][3], Chest)):
                for f in map[self.x][self.y + 1][3].invent:
                    print(str(f.name) + " с уроном " + str(f.dmg))
                hjh = int(input())
                opo = map[self.x][self.y + 1][3].invent[hjh]
                if (len(self.invent) < self.inv):
                    self.invent.append(opo)
                    del map[self.x][self.y + 1][3].invent[hjh]
            elif (isinstance(map[self.x][self.y - 1][3], Chest)):
                for f in map[self.x][self.y - 1][3].invent:
                    print(str(f.name) + " с уроном " + str(f.dmg))
                hjh = int(input())
                opo = map[self.x][self.y - 1][3].invent[hjh]
                if (len(self.invent) < self.inv):
                    self.invent.append(opo)
                    del map[self.x][self.y - 1][3].invent[hjh]
        if (p[0] == "обыскать"):
            if (random.randint(1, 15) == 5):
                print("Нашел!")
                if (len(self.invent) < self.inv):
                    self.invent.append(randthing())
            else:
                print("Ничего нет")
        if (p[0] == "искать"):
            if (random.randint(1, 4) == 2):
                print("Нашел!")
                if (map[self.x][self.y][0] == 1):
                    xxx = lug[random.randint(0, len(lug) - 1)]
                elif (map[self.x][self.y][0] == 2):
                    xxx = forest[random.randint(0, len(forest) - 1)]
                elif (map[self.x][self.y][0] == 3):
                    if(not self.findinv("Кирка")):
                        self.ener = self.ener - 10
                    if(random.randint(0, 3)==1):
                       xxx = rare[random.randint(0, len(rare) - 1)]
                    else:
                        xxx = mount[random.randint(0, len(mount) - 1)]
                else:
                    if (map[self.x + 1][self.y][0]==4):
                        xxx =water[random.randint(0,len(water)-1)]
                    elif (map[self.x - 1][self.y][0]==4):
                        xxx = water[random.randint(0, len(water) - 1)]
                    elif (map[self.x][self.y + 1][0]==4):
                        xxx = water[random.randint(0, len(water) - 1)]
                    elif (map[self.x][self.y - 1][0]==4):
                        xxx = water[random.randint(0, len(water) - 1)]
                if not(map[self.x][self.y][0] == 1 or map[self.x][self.y][0] == 2 or map[self.x][self.y][0] == 3):
                    xxx = findable[random.randint(0, len(findable) - 1)]
                xxx2 = Thing(xxx,2,1)
                self.invent.append(xxx2)
            else:
                print("Ничего нет")

        if (p[0] == "идти"):
            if (p[1] == "вниз" and (not isinstance(map[self.x + 1][self.y][3], Wall)) and (self.ener>0) and (
                    map[self.x + 1][self.y][0] != 4 or map[self.x + 1][self.y][1] != 0 or self.findinv("Лодка"))):
                if (len(p) == 2):
                    if(self.friend == "0"):
                        self.ener = self.ener - 1
                    movemap(self.x, self.y, 4, self.x + 1, self.y, 4)
                    self.x = self.x + 1
                elif (p[2] == "на"):
                    for j in range(0, int(p[3])):
                        if (p[1] == "вниз" and (not isinstance(map[self.x + 1][self.y][3], Wall)) and (self.ener>0)  and (
                                map[self.x + 1][self.y][0] != 4 or map[self.x + 1][self.y][1] != 0 or self.findinv("Лодка"))):
                            if (self.friend == "0"):
                                self.ener = self.ener - 1
                            movemap(self.x, self.y, 4, self.x + 1, self.y, 4)
                            self.x = self.x + 1
            if (p[1] == "вверх" and (not isinstance(map[self.x - 1][self.y][3], Wall)) and (self.ener>0)  and (
                    map[self.x - 1][self.y][0] != 4 or map[self.x - 1][self.y][1] != 0 or self.findinv("Лодка"))):
                if (len(p) == 2):
                    if (self.friend == "0"):
                        self.ener = self.ener - 1
                    movemap(self.x, self.y, 4, self.x - 1, self.y, 4)
                    self.x = self.x - 1
                elif (p[2] == "на"):
                    for j in range(0, int(p[3])):
                        if (p[1] == "вверх" and (not isinstance(map[self.x - 1][self.y][3], Wall)) and (self.ener>0)  and (
                                map[self.x - 1][self.y][0] != 4 or map[self.x - 1][self.y][1] != 0 or self.findinv("Лодка"))):
                            movemap(self.x, self.y, 4, self.x - 1, self.y, 4)
                            if (self.friend == "0"):
                                self.ener = self.ener - 1
                            self.x = self.x - 1
            if (p[1] == "вправо" and (not isinstance(map[self.x][self.y + 1][3], Wall)) and (self.ener>0)  and (
                    map[self.x][self.y + 1][0] != 4 or map[self.x][self.y + 1][1] != 0 or self.findinv("Лодка"))):
                if (len(p) == 2):
                    if (self.friend == "0"):
                        self.ener = self.ener - 1
                    movemap(self.x, self.y, 4, self.x, self.y + 1, 4)
                    self.y = self.y + 1
                elif (p[2] == "на"):
                    for j in range(0, int(p[3])):
                        if (p[1] == "вправо" and (not isinstance(map[self.x][self.y + 1][3], Wall)) and (self.ener>0)  and (
                                map[self.x][self.y + 1][0] != 4 or map[self.x][self.y + 1][1] != 0 or self.findinv("Лодка"))):
                            if (self.friend == "0"):
                                self.ener = self.ener - 1
                            movemap(self.x, self.y, 4, self.x, self.y + 1, 4)
                            self.y = self.y + 1
            if (p[1] == "влево" and (not isinstance(map[self.x][self.y - 1][3], Wall) and (self.ener>0) ) and (
                    map[self.x][self.y - 1][0] != 4 or map[self.x][self.y - 1][1] != 0 or self.findinv("Лодка"))):
                if (len(p) == 2):
                    movemap(self.x, self.y, 4, self.x, self.y - 1, 4)
                    self.y = self.y - 1
                    if (self.friend == "0"):
                        self.ener = self.ener - 1
                elif (p[2] == "на"):
                    for j in range(0, int(p[3])):
                        if (p[1] == "влево" and (not isinstance(map[self.x][self.y - 1][3], Wall)) and (self.ener>0)  and (
                                map[self.x][self.y - 1][0] != 4 or map[self.x][self.y - 1][1] != 0 or self.findinv("Лодка"))):
                            movemap(self.x, self.y, 4, self.x, self.y - 1, 4)
                            if (self.friend == "0"):
                                self.ener = self.ener - 1
                            self.y = self.y - 1
        if (p[0] == "перелезть"):
            if (p[1] == "стену"):
                if (isinstance(map[self.x + 1][self.y][3], Wall)and self.ener>9):
                    movemap(self.x, self.y, 4, self.x + 2, self.y, 4)
                    self.x = self.x + 2
                    self.ener = self.ener - 10
                    kingdoms[map[self.x + 1][self.y][2]].relations = kingdoms[map[self.x + 1][self.y][2]].relations - 10
                elif (isinstance(map[self.x - 1][self.y][3], Wall)and self.ener>9):
                    movemap(self.x, self.y, 4, self.x - 2, self.y, 4)
                    self.x = self.x - 2
                    self.ener = self.ener - 10
                    kingdoms[map[self.x - 1][self.y][2]].relations = kingdoms[map[self.x - 1][self.y][2]].relations - 10
                elif (isinstance(map[self.x][self.y + 1][3], Wall)and self.ener>9):
                    movemap(self.x, self.y, 4, self.x, self.y + 2, 4)
                    self.y = self.y + 2
                    self.ener = self.ener - 10
                    kingdoms[map[self.x][self.y + 1][2]].relations = kingdoms[map[self.x][self.y + 1][2]].relations - 10
                elif (isinstance(map[self.x][self.y - 1][3], Wall)and self.ener>9):
                    movemap(self.x, self.y, 4, self.x, self.y - 2, 4)
                    self.y = self.y - 2
                    self.ener = self.ener - 10
                    kingdoms[map[self.x][self.y - 1][2]].relations = kingdoms[map[self.x][self.y - 1][2]].relations - 10
        if (p[0] == "охотиться"):
            if (p[2] == "животных"):
                if (isinstance(map[self.x + 1][self.y][3], Animal)):
                    self.fight(map[self.x + 1][self.y][3])
                elif (isinstance(map[self.x - 1][self.y][3], Animal)):
                    self.fight(map[self.x - 1][self.y][3])
                elif (isinstance(map[self.x][self.y + 1][3], Animal)):
                    self.fight(map[self.x][self.y + 1][3])
                elif (isinstance(map[self.x][self.y - 1][3], Animal)):
                    self.fight(map[self.x][self.y - 1][3])
        if (p[0] == "приручить"):
            if (p[1] == "животных" and (self.findinv3("Яблоко")!=-1)):
                del self.invent[self.findinv3("Яблоко")]
                if (isinstance(map[self.x + 1][self.y][3], Animal)):
                    self.friend = map[self.x + 1][self.y][3]
                    map[self.x + 1][self.y][3] = 0
                elif (isinstance(map[self.x - 1][self.y][3], Animal)):
                    self.friend = map[self.x - 1][self.y][3]
                    map[self.x - 1][self.y][3] = 0
                elif (isinstance(map[self.x][self.y + 1][3], Animal)):
                    self.friend = map[self.x][self.y + 1][3]
                    map[self.x][self.y + 1][3] = 0
                elif (isinstance(map[self.x][self.y - 1][3], Animal)):
                    self.friend = map[self.x][self.y - 1][3]
                    map[self.x][self.y - 1][3] = 0
        if (p[0] == "срубить"):
            if (p[1] == "дерево"):
                if(not self.findinv("Топор")):
                    self.ener = self.ener - 10
                if (isinstance(map[self.x + 1][self.y][3], Tree)):
                    map[self.x + 1][self.y][3] = "0"
                    th = Thing("Брусок дерева", 2, 1)
                    if (len(self.invent) < self.inv):
                        self.invent.append(th)
                    if random.randint(1,2)==2:
                        th = Thing("Яблоко", 4, 1)
                        if (len(self.invent) < self.inv):
                            self.invent.append(th)
                elif (isinstance(map[self.x - 1][self.y][3], Tree)):
                    map[self.x - 1][self.y][3] = "0"
                    th = Thing("Брусок дерева", 2, 1)
                    if (len(self.invent) < self.inv):
                        self.invent.append(th)
                    if random.randint(1,2)==2:
                        th = Thing("Яблоко", 4, 1)
                        if (len(self.invent) < self.inv):
                            self.invent.append(th)
                elif (isinstance(map[self.x][self.y + 1][3], Tree)):
                    map[self.x][self.y + 1][3] = "0"
                    th = Thing("Брусок дерева", 2, 1)
                    if (len(self.invent) < self.inv):
                        self.invent.append(th)
                    if random.randint(1,2)==2:
                        th = Thing("Яблоко", 4, 1)
                        if (len(self.invent) < self.inv):
                            self.invent.append(th)
                elif (isinstance(map[self.x][self.y - 1][3], Tree)):
                    map[self.x][self.y - 1][3] = "0"
                    th = Thing("Брусок дерева", 2, 1)
                    if (len(self.invent) < self.inv):
                        self.invent.append(th)
                    if random.randint(1,2)==2:
                        th = Thing("Яблоко", 4, 1)
                        if (len(self.invent) < self.inv):
                            self.invent.append(th)
        if (p[0] == "поговорить"):
            if (isinstance(map[self.x + 1][self.y][4], NPC)):
                self.talk(map[self.x + 1][self.y][4])
            elif (isinstance(map[self.x - 1][self.y][4], NPC)):
                self.talk(map[self.x - 1][self.y][4])
            elif (isinstance(map[self.x][self.y + 1][4], NPC)):
                self.talk(map[self.x][self.y + 1][4])
            elif (isinstance(map[self.x][self.y - 1][4], NPC)):
                self.talk(map[self.x][self.y - 1][4])
        if (p[0] == "подраться"):
            if (isinstance(map[self.x + 1][self.y][4], NPC)):
                self.fight(map[self.x + 1][self.y][4])
            elif (isinstance(map[self.x - 1][self.y][4], NPC)):
                self.fight(map[self.x - 1][self.y][4])
            elif (isinstance(map[self.x][self.y + 1][4], NPC)):
                self.fight(map[self.x][self.y + 1][4])
            elif (isinstance(map[self.x][self.y - 1][4], NPC)):
                self.fight(map[self.x][self.y - 1][4])
        if (p[0] == "инвентарь"):
            for f in self.invent:
                print(str(f.name) + " с уроном " + str(f.dmg))
        if (p[0] == "использовать"):
            g = 0
            for f in self.invent:
                print(str(g) + ") " + str(f.name) + " с уроном " + str(f.dmg))
                g = g + 1
            hhh = int(input())
            isp = self.invent[hhh]
            cec2 = 0
            enerp = 0
            if(isp.name in foods):
                for cec in foods:
                    if (isp.name == cec):
                        enerp = random.randint(foods2[cec2][1] - 2, foods2[cec2][1] + 2)
                    cec2 = cec2 + 1
                self.ener = self.ener + enerp
                self.hp = self.hp + enerp
            del self.invent[hhh]
            if(isp.name=="Кирпич"):
                tfc = House(200,4)
                map[self.x][self.y][3]=tfc
                if(not self.findinv("Молот")):
                    self.ener = self.ener - 10
            if (isp.name == "Сундук"):
                tfc = Chest(5)
                map[self.x][self.y][3] = tfc
        if (p[0] == "крафт"):
            g = 0
            for f in self.invent:
                print(str(g) + ") " + str(f.name) + " с уроном " + str(f.dmg))
                g = g + 1
            hhh = int(input())
            hhh2 = int(input())
            craf = self.invent[hhh].name
            craft = self.invent[hhh2].name
            for cec in binds:
                if (cec[0] == craf and cec[1] == craft) or (cec[1] == craf and cec[0] == craft):
                    if (hhh > hhh2):
                        del self.invent[hhh2]
                        del self.invent[hhh - 1]
                    else:
                        del self.invent[hhh]
                        del self.invent[hhh2 - 1]
                        bit = ""
                        if(cec[2] in weapons):
                            bit = Thing(cec[2],1,1)
                        if (cec[2] in foods):
                            bit = Thing(cec[2], 4, 1)
                        if (cec[2] in magics):
                            bit = Thing(cec[2], 3, 1)
                        if (cec[2] in crafts):
                            bit = Thing(cec[2], 2, 1)
                        if (cec[2] in uses):
                            bit = Thing(cec[2], 2, 1)
                    self.invent.append(bit)
        if(self.ener<0):
            print("Вы уснули от усталости")
            time.sleep(10)

map = [[]]


def movemap(x, y, z, x2, y2, z2):  # Функция перемищения объектов
    if (map[x2][y2][z2] == " "):
        obj = map[x][y][z]
        map[x][y][z] = " "
        map[x2][y2][z2] = obj
    else:
        obj = map[x][y][z]
        map[x][y][z] = map[x2][y2][z2]
        map[x2][y2][z2] = obj


def mapGen(x, y, bioms):
    for a in range(0, x):  # Заполнение мира пустыней
        for c in range(0, y):
            map[a].append([])
            map[a][c].append(8)
        map.append([])
    del map[x]
    for a in range(0, bioms):
        k1 = random.randint(0, x)
        k2 = random.randint(0, y)
        k3 = random.randint(k1, x)
        k4 = random.randint(k2, y)
        ty = random.randint(1, 3)
        for d1 in range(k1, k3):
            for d2 in range(k2, k4):  # Заполнениие мира биомами и их наложением
                if (map[d1][d2][0] == 8):
                    map[d1][d2][0] = ty
                if (map[d1][d2][0] == 2 and ty == 1 or map[d1][d2][0] == 1 and ty == 2):
                    map[d1][d2][0] = 4
                if (map[d1][d2][0] == 3 and ty == 1 or map[d1][d2][0] == 1 and ty == 3):
                    map[d1][d2][0] = 5
                if (map[d1][d2][0] == 2 and ty == 3 or map[d1][d2][0] == 3 and ty == 2):
                    map[d1][d2][0] = 6
                if (map[d1][d2][0] == 4 and ty == 3 or map[d1][d2][0] == 4 and ty == 2):
                    map[d1][d2][0] = 7


def castleGen(x, y, bioms, bioms2):
    qqq = ""
    for a in range(0, x):  # Заполнение мира
        for c in range(0, y):
            map[a].append([])
            map[a][c].append(0)
            map[a][c].append(0)
            map[a][c].append(0)
            map[a][c].append(0)
        map.append([])
    del map[x]
    for a in range(0, bioms2):
        k1 = random.randint(2, int(x / 3 - 4))
        k2 = random.randint(2, int(y / 3 - 4))
        for d1 in range(k1 * 3, k1 * 3 + 3):
            for d2 in range(k2 * 3, k2 * 3 + 3):
                z = House(100, map[d1][d2][1])
                map[d1][d2][3] = z
            fam = ["Петров", "Карлов", "Михов", "Васьков", "Александров"]
            nam = ["Петр", "Карл", "Миха", "Вася", "Александр"]
            z = NPC(nam[random.randint(0, 4)] + " " + fam[random.randint(0, 4)], 2, 100, 0, 0, 2, k1 * 3 + 1,
                    k2 * 3 + 1, 0)
            map[k1 * 3 + 1][k2 * 3 + 1][4] = z
    for a in range(0, bioms):
        k1 = random.randint(2, x / 5 - 4)
        k2 = random.randint(2, y / 5 - 4)
        if (x / 5 - k1 > 5):
            k3 = random.randint(k1, k1 + 5)
        else:
            k3 = random.randint(k1, x / 5 - 2)
        if (y / 5 - k2 > 5):
            k4 = random.randint(k2, k2 + 4)
        else:
            k4 = random.randint(k2, y / 5 - 2)
        ty = random.randint(1, 3)
        cas = random.randint(1, 9)
        for d1 in range(k1 * 5, k3 * 5):
            for d2 in range(k2 * 5, k4 * 5):  # Заполнениие мира замками
                map[d1][d2][1] = ty
                map[d1][d2][2] = cas
        for d1 in range(k1 * 5, k3 * 5):
            for d2 in range(k2 * 5, k4 * 5):
                if (d1 == k1 * 5 or d1 == k3 * 5 - 1 or d2 == k2 * 5 or d2 == k4 * 5 - 1 and(d1!=((k3-k1))+k1*5)):
                    z = Wall(200 * map[d1][d2][1], map[d1][d2][1], cas)
                    map[d1][d2][3] = z
        for d1 in range(k1 * 5 + 1, k3 * 5 - 1):
            for d2 in range(k2 * 5 + 1, k4 * 5 - 1):
                if (not (d1 % 4 == 0 or d2 % 4 == 0 ))and(d1!=((k3-k1))+k1*5)and not(d1 == k1 * 5 or d1 == k3 * 5 - 1 or d2 == k2 * 5 or d2 == k4 * 5 - 1 ):
                    z = House(100 * map[d1][d2][1], map[d1][d2][1])
                    map[d1][d2][3] = z
                else:
                    map[d1][d2][3] = " "
                if (d1 % 2 == 0 or d1 % 4 == 0) and (d2 % 2 == 0 or d2 % 4 == 0) and isinstance(map[d1][d2][3], House)and not(d1 == k1 * 5 or d1 == k3 * 5 - 1 or d2 == k2 * 5 or d2 == k4 * 5 - 1 ):
                    fam = ["Петров", "Карлов", "Михов", "Васьков", "Александров"]
                    nam = ["Петр", "Карл", "Миха", "Вася", "Александр"]
                    if (random.randint(0, 5) == 5):
                        z = NPC(nam[random.randint(0, 4)] + " " + fam[random.randint(0, 4)], 2, 100, 0, 0, 2, d1, d2,
                                cas)
                    else:
                        z = NPC(nam[random.randint(0, 4)] + " " + fam[random.randint(0, 4)], 2, 100, 0, 0, 2, d1, d2,
                                cas)
                    map[d1][d2][4] = z
                    qqq = z
    return qqq


def AnimalGen(x, y):
    for d1 in range(1, x - 1):
        for d2 in range(1, y - 1):
            if (map[d1][d2][0] == 6 and map[d1][d2][1] == 0 and not isinstance(map[d1][d2][3], House)):
                if random.randint(0, 4) == 4:
                    map[d1][d2][3] = Animal("b",d1,d2)
            if (map[d1][d2][0] == 2 and map[d1][d2][1] == 0 and not isinstance(map[d1][d2][3], House)):
                if random.randint(0, 4) == 4:
                    map[d1][d2][3] = Animal("m",d1,d2)
            if ((map[d1][d2][0] == 2 or map[d1][d2][0] == 7 or map[d1][d2][0] == 6) and map[d1][d2][1] == 0 and not isinstance(map[d1][d2][3], House)):
                if random.randint(0, 4) == 4:
                    map[d1][d2][3] = Tree()
            if (map[d1][d2][0] == 4 and map[d1][d2][1] == 0 and not isinstance(map[d1][d2][3], House)):
                if random.randint(0, 4) == 4:
                    map[d1][d2][3] = Animal("f",d1,d2)
            if (map[d1][d2][0] == 1 and map[d1][d2][1] == 0 and not isinstance(map[d1][d2][3], House)):
                if random.randint(0, 4) == 4:
                    map[d1][d2][3] = Animal("r",d1,d2)
            if (map[d1][d2][0] == 7 and map[d1][d2][1] == 0 and not isinstance(map[d1][d2][3], House)):
                if random.randint(0, 4) == 4:
                    map[d1][d2][3] = Animal("h",d1,d2)




kingdoms = []
print("Ждите...")
thp = Thing("Кулаки",1,"Удар ")
for s12 in range(-1, 10):
    ll2 = Kingdom(s12)
    kingdoms.append(ll2)
mapGen(1000, 1000, 1000)
castleGen(1000, 1000, random.randint(50, 100), random.randint(60, 120))
AnimalGen(1000, 1000)
uuu = ""
fam = ["Петров", "Карлов", "Михов", "Васьков", "Александров"]
nam = ["Петр", "Карл", "Миха", "Вася", "Александр"]
z = NPC(nam[random.randint(0, 4)] + " " + fam[random.randint(0, 4)], 1, 100, 0, 0, 2, 1, 1, 0)
map[1][1][4] = z
z = Hero(1, 200, 100, 1, 1, 20, 100,500,500)
z.invent.append(thp)
map[500][500][4] = z
qwst = []
print("Готово!")
while (uuu != "exit"):
    uuu = input()
    z.logic(uuu)
    for bin in range(0, len(npsive) - 1):
        npsive[bin].logic()
    for v in range(1, 2):
        for a in range(z.x-7, z.x+8):
            bit = ""
            for c in range(z.y-7, z.y+8):
                if (len(str(map[a][c][4])) != 1):
                    bit = bit + map[a][c][4].wrd
                else:
                    if (len(str(map[a][c][3])) == 1):
                        bit = bit + str(map[a][c][3])
                    else:
                        bit = bit + map[a][c][3].wrd
                if (isinstance(map[a][c][3], Animal)):
                    map[a][c][3].pair(a, c)
            print(bit)
        print("")
